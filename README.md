Kraken taxonomic sequence classification system
===============================================

Vedere [Kraken webpage] o [Kraken manual] per informazioni sull'installazione e le operazioni concesse da Kraken.

Per scaricare Kraken e il database da 4GB MiniKraken sul proprio dispositivo:
[Kraken webpage]:   http://ccb.jhu.edu/software/kraken/

Per consultare il manuale:
[Kraken manual]:    http://ccb.jhu.edu/software/kraken/MANUAL.html

I dataset del progetto da analizzare, ossia HiSeq, MiSeq e SimBA-5 sono da reperire al sito:
[Dataset progetto]: https://bitbucket.org/samu661/clior

Non sono stati caricati all'interno di questa repository per questioni di pesantezza (più di 8GB).

Comandi per eseguire il nostro codice:

1) Per compilare il nostro eseguibile, aprire il terminale all'interno della repository globale del progetto (in cui deve essere installata 
anche la repository del tool Kraken) e lanciare il comando:

	./install_kraken.sh $Kraken

dove $Kraken è la repository in cui viene installato il tool.

2) Per generare i file di output lanciare successivamente il comando:
	
	kraken --preload --db $DBNAME seqs.fa 

per tutti i dataset, dove $DBNAME è la repository in cui viene salvato il database mentre seqs.fa è il nome del file di input su cui eseguire i test. Se non si vuole sovrascrivere progressivamente il file di output, rinominare poi ogni esito. Un esempio della sequenza da noi lanciata, dopo aver scaricato i dataset nella cartella dati e installato il tool nella cartella kraken è:

- Kraken/kraken --preload --db minikraken_20171013_4GB Dati/HiSeq_timing_part01.fa > output.kraken

- Kraken/kraken --preload --db minikraken_20171013_4GB Dati/HiSeq_timing_part02.fa > output.kraken

- Kraken/kraken --preload --db minikraken_20171013_4GB Dati/HiSeq_timing_part03.fa > output.kraken

- Kraken/kraken --preload --db minikraken_20171013_4GB Dati/HiSeq_timing_part04.fa > output.kraken

- Kraken/kraken --preload --db minikraken_20171013_4GB Dati/MiSeq_timing_part03.fa.4000000 > output.kraken

- Kraken/kraken --preload --db minikraken_20171013_4GB Dati/MiSeq_timing_part_04.fa.4000000 > output.kraken

- Kraken/kraken --preload --db minikraken_20171013_4GB Dati/simBA5_timing_part02.fa.3000000 Dati/simBA5_timing_part03.fa.3000000 Dati/simBA5_timing_part04.fa.3000000 > output.kraken

in cui output.kraken memorizza in un file di testo l'output standard di Kraken.


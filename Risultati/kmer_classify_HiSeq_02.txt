
Classificazione kmers HiSeq_02

Totale kmers trovati nel file in input: 623974662
Kmers classificati: 10132118 (1.6238 % )
Kmers non classificati: 613842544 (98.3762 % )

Kmers classificati livello specie: 7163781 (70.7037 % )

>Bacteroides fragilis	taxID: 817
	1147202	|	% 16.014
>Staphylococcus aureus	taxID: 1280
	1131946	|	% 15.801
>Mycobacterium chelonae group	taxID: 670516
	1089820	|	% 15.213
>Rhodobacter sphaeroides	taxID: 1063
	883973	|	% 12.339
>Vibrio cholerae	taxID: 666
	716560	|	% 10.003
>Streptococcus pneumoniae	taxID: 1313
	666670	|	% 9.306
>Bacillus cereus group	taxID: 86661
	593277	|	% 8.282
>Xanthomonas phaseoli	taxID: 1985254
	280061	|	% 3.909
>Pelosinus fermentans	taxID: 365349
	244623	|	% 3.415
>Aeromonas hydrophila	taxID: 644
	150871	|	% 2.106
>Aeromonas dhakensis	taxID: 196024
	98514	|	% 1.375
>Xanthomonas citri group	taxID: 643453
	72286	|	% 1.009
>Aeromonas veronii	taxID: 654
	8156	|	% 0.114
>Aeromonas sp. O23A	taxID: 697046
	7634	|	% 0.107
>Chryseobacterium gallinarum	taxID: 1324352
	5768	|	% 0.081
>Enterococcus faecium	taxID: 1352
	4740	|	% 0.066
>Xanthomonas oryzae	taxID: 347
	4093	|	% 0.057
>Bacteroides coprosuis	taxID: 151276
	3887	|	% 0.054
>Bacteroides caecimuris	taxID: 1796613
	3722	|	% 0.052
>Aeromonas aquatica	taxID: 558964
	3214	|	% 0.045
>Xanthomonas campestris	taxID: 339
	2926	|	% 0.041
>Pelosinus sp. UFO1	taxID: 484770
	2775	|	% 0.039
>Bacteroides thetaiotaomicron	taxID: 818
	2757	|	% 0.038
>Xanthomonas euvesicatoria	taxID: 456327
	2115	|	% 0.030
>Aeromonas schubertii	taxID: 652
	1952	|	% 0.027
>Xanthomonas cassavae	taxID: 56450
	1841	|	% 0.026
>Enterococcus faecalis	taxID: 1351
	1776	|	% 0.025
>Xanthomonas vesicatoria	taxID: 56460
	1768	|	% 0.025
>Lactococcus lactis	taxID: 1358
	1680	|	% 0.023
>Xanthomonas alfalfae	taxID: 366650
	1567	|	% 0.022
>Aeromonas media	taxID: 651
	1524	|	% 0.021
>Aeromonas salmonicida	taxID: 645
	1191	|	% 0.017
>Escherichia coli	taxID: 562
	1177	|	% 0.016
>Pseudomonas aeruginosa group	taxID: 136841
	1088	|	% 0.015
>Edwardsiella ictaluri	taxID: 67780
	953	|	% 0.013
>Bacteroides dorei	taxID: 357276
	949	|	% 0.013
>[Clostridium] saccharolyticum	taxID: 84030
	848	|	% 0.012
>Thauera sp. K11	taxID: 2005884
	800	|	% 0.011
>Bacteroides ovatus	taxID: 28116
	728	|	% 0.010
>Xanthomonas fragariae	taxID: 48664
	698	|	% 0.010
>Stenotrophomonas maltophilia	taxID: 40324
	614	|	% 0.009
>Xanthomonas arboricola	taxID: 56448
	608	|	% 0.008
>Bacteroides salanitronis	taxID: 376805
	572	|	% 0.008
>Bacteroides cellulosilyticus	taxID: 246787
	535	|	% 0.007
>Xanthomonas hortorum	taxID: 56454
	512	|	% 0.007
>Fusobacterium nucleatum	taxID: 851
	502	|	% 0.007
>Mycobacterium tuberculosis complex	taxID: 77643
	471	|	% 0.007
>Xanthomonas perforans	taxID: 442694
	445	|	% 0.006
>Ornithobacterium rhinotracheale	taxID: 28251
	440	|	% 0.006
>Bacillus sp. FDAARGOS_235	taxID: 1839798
	341	|	% 0.005
>Cutibacterium acnes	taxID: 1747
	325	|	% 0.005
>Xanthomonas translucens	taxID: 343
	303	|	% 0.004
>Pandoraea pnomenusa	taxID: 93220
	284	|	% 0.004
>Treponema denticola	taxID: 158
	274	|	% 0.004
>Prevotella melaninogenica	taxID: 28132
	264	|	% 0.004
>Myroides odoratimimus	taxID: 76832
	249	|	% 0.003
>Alistipes shahii	taxID: 328814
	240	|	% 0.003
>Lactobacillus jensenii	taxID: 109790
	227	|	% 0.003
>Pseudomonas syringae group	taxID: 136849
	201	|	% 0.003
>Stenotrophomonas rhizophila	taxID: 216778
	200	|	% 0.003
>Bacillus simplex	taxID: 1478
	199	|	% 0.003
>Pseudomonas fluorescens group	taxID: 136843
	197	|	% 0.003
>Acinetobacter calcoaceticus/baumannii complex	taxID: 909768
	181	|	% 0.003
>Enterobacter cloacae complex	taxID: 354276
	181	|	% 0.003
>Streptococcus mitis	taxID: 28037
	174	|	% 0.002
>Acidovorax sp. NA3	taxID: 553814
	168	|	% 0.002
>Pseudomonas sp. ATCC 13867	taxID: 1294143
	151	|	% 0.002
>Xanthomonas gardneri	taxID: 90270
	151	|	% 0.002
>Devosia sp. H5989	taxID: 1643450
	146	|	% 0.002
>Xanthomonas sacchari	taxID: 56458
	127	|	% 0.002
>Alicycliphilus denitrificans	taxID: 179636
	118	|	% 0.002
>Paucibacter sp. KCTC 42545	taxID: 1768242
	116	|	% 0.002
>Parabacteroides sp. CT06	taxID: 2025876
	105	|	% 0.001
>Zobellella denitrificans	taxID: 347534
	101	|	% 0.001
>Bacillus sp. ABP14	taxID: 1892404
	97	|	% 0.001
>Bacteroides vulgatus	taxID: 821
	97	|	% 0.001
>Xanthomonas albilineans	taxID: 29447
	95	|	% 0.001
>Bifidobacterium bifidum	taxID: 1681
	94	|	% 0.001
>Pseudoxanthomonas spadix	taxID: 415229
	89	|	% 0.001
>Pseudoxanthomonas suwonensis	taxID: 314722
	78	|	% 0.001
>Bacteroides helcogenes	taxID: 290053
	78	|	% 0.001
>Vibrio sp. 2521-89	taxID: 2014742
	77	|	% 0.001
>Staphylococcus epidermidis	taxID: 1282
	75	|	% 0.001
>Oceanimonas sp. GK1	taxID: 511062
	71	|	% 0.001
>Bordetella parapertussis	taxID: 519
	71	|	% 0.001
>Streptococcus pseudopneumoniae	taxID: 257758
	67	|	% 0.001
>Sphingomonas melonis	taxID: 152682
	65	|	% 0.001
>Streptococcus oralis	taxID: 1303
	65	|	% 0.001
>Thiomonas intermedia	taxID: 926
	61	|	% 0.001
>Pseudomonas putida group	taxID: 136845
	60	|	% 0.001
>Lysobacter antibioticus	taxID: 84531
	60	|	% 0.001
>Bordetella pseudohinzii	taxID: 1331258
	59	|	% 0.001
>Bacillus subtilis group	taxID: 653685
	59	|	% 0.001
>Clostridium cochlearium	taxID: 1494
	58	|	% 0.001
>Stenotrophomonas sp. LM091	taxID: 1904944
	57	|	% 0.001
>Vibrio mimicus	taxID: 674
	57	|	% 0.001
>Staphylococcus argenteus	taxID: 985002
	55	|	% 0.001
>Blautia hansenii	taxID: 1322
	52	|	% 0.001
>Lysobacter capsici	taxID: 435897
	50	|	% 0.001
>Pseudomonas stutzeri group	taxID: 136846
	49	|	% 0.001
>Lysobacter enzymogenes	taxID: 69
	47	|	% 0.001
>Paenibacillus sonchi group	taxID: 2044880
	45	|	% 0.001
>Stenotrophomonas acidaminiphila	taxID: 128780
	44	|	% 0.001
>pseudomallei group	taxID: 111527
	44	|	% 0.001
>Clostridioides difficile	taxID: 1496
	43	|	% 0.001
>Vibrio harveyi group	taxID: 717610
	42	|	% 0.001
>Burkholderia cepacia complex	taxID: 87882
	42	|	% 0.001
>Enterobacteria phage phiX174 sensu lato	taxID: 374840
	40	|	% 0.001
>Bacillus sp. FJAT-22090	taxID: 1581038
	38	|	% 0.001
>Solibacillus silvestris	taxID: 76853
	38	|	% 0.001
>Roseburia hominis	taxID: 301301
	37	|	% 0.001
>Myroides odoratus	taxID: 256
	36	|	% 0.001
>Sphingomonas sanxanigenens	taxID: 397260
	35	|	% 0.000
>Staphylococcus virus 29	taxID: 320846
	35	|	% 0.000
>Alcanivorax xenomutans	taxID: 1094342
	34	|	% 0.000
>Dyella japonica	taxID: 231455
	34	|	% 0.000
>Comamonas testosteroni	taxID: 285
	34	|	% 0.000
>Spiribacter salinus	taxID: 1335746
	32	|	% 0.000
>Dickeya zeae	taxID: 204042
	31	|	% 0.000
>Burkholderia plantarii	taxID: 41899
	31	|	% 0.000
>Marinobacterium aestuarii	taxID: 1821621
	30	|	% 0.000
>Aneurinibacillus soli	taxID: 1500254
	30	|	% 0.000
>Pseudomonas chlororaphis group	taxID: 136842
	29	|	% 0.000
>Bacillus bombysepticus	taxID: 658666
	28	|	% 0.000
>Serratia fonticola	taxID: 47917
	27	|	% 0.000
>Lachnoclostridium phocaeense	taxID: 1871021
	26	|	% 0.000
>Bacillus muralis	taxID: 264697
	26	|	% 0.000
>Rhodoferax antarcticus	taxID: 81479
	26	|	% 0.000
>Pseudomonas sp. MRSN12121	taxID: 1611770
	24	|	% 0.000
>Rummeliibacillus stabekisii	taxID: 241244
	24	|	% 0.000
>Alistipes finegoldii	taxID: 214856
	24	|	% 0.000
>Clostridium kluyveri	taxID: 1534
	24	|	% 0.000
>Streptococcus suis	taxID: 1307
	24	|	% 0.000
>Streptomyces sp. Tue 6075	taxID: 1661694
	23	|	% 0.000
>Acinetobacter equi	taxID: 1324350
	23	|	% 0.000
>Serratia marcescens	taxID: 615
	23	|	% 0.000
>Sphingomonas panacis	taxID: 1560345
	22	|	% 0.000
>Alteromonas mediterranea	taxID: 314275
	22	|	% 0.000
>Lactobacillus plantarum	taxID: 1590
	22	|	% 0.000
>Brevibacillus laterosporus	taxID: 1465
	22	|	% 0.000
>Luteimonas sp. JM171	taxID: 1896164
	21	|	% 0.000
>Chania multitudinisentens	taxID: 1639108
	21	|	% 0.000
>Lactobacillus backii	taxID: 375175
	21	|	% 0.000
>Nitrosomonas ureae	taxID: 44577
	21	|	% 0.000
>Ralstonia solanacearum	taxID: 305
	21	|	% 0.000
>Luteimonas sp. 100111	taxID: 2006110
	20	|	% 0.000
>Thauera humireducens	taxID: 1134435
	20	|	% 0.000
>Paenibacillus sp. JDR-2	taxID: 324057
	20	|	% 0.000
>Megamonas hypermegale	taxID: 158847
	20	|	% 0.000
>Ramlibacter tataouinensis	taxID: 94132
	20	|	% 0.000
>Frateuria aurantia	taxID: 81475
	20	|	% 0.000
>Anoxybacillus flavithermus	taxID: 33934
	20	|	% 0.000
>Methanohalobium evestigatum	taxID: 2322
	20	|	% 0.000
>Rhodobacter capsulatus	taxID: 1061
	20	|	% 0.000
>Brenneria goodwinii	taxID: 1109412
	19	|	% 0.000
>Alcanivorax dieselolei	taxID: 285091
	19	|	% 0.000
>Pandoraea norimbergensis	taxID: 93219
	19	|	% 0.000
>Bacillus flexus	taxID: 86664
	19	|	% 0.000
>Parabacteroides distasonis	taxID: 823
	19	|	% 0.000
>Virgibacillus sp. 6R	taxID: 1911587
	18	|	% 0.000
>Marinobacter sp. LQ44	taxID: 1749259
	18	|	% 0.000
>Rhizobacter gummiphilus	taxID: 946333
	18	|	% 0.000
>Sphingopyxis alaskensis	taxID: 117207
	18	|	% 0.000
>Clostridium beijerinckii	taxID: 1520
	18	|	% 0.000
>Jeongeupia sp. USM3	taxID: 1906741
	17	|	% 0.000
>Pseudomonas saudiphocaensis	taxID: 1499686
	17	|	% 0.000
>Ochrobactrum pseudogrignonense	taxID: 419475
	17	|	% 0.000
>Xenorhabdus doucetiae	taxID: 351671
	17	|	% 0.000
>Bacillus krulwichiae	taxID: 199441
	17	|	% 0.000
>Pseudarthrobacter chlorophenolicus	taxID: 85085
	17	|	% 0.000
>Salmonella enterica	taxID: 28901
	17	|	% 0.000
>Listeria monocytogenes	taxID: 1639
	17	|	% 0.000
>Clostridium tetani	taxID: 1513
	17	|	% 0.000
>Streptococcus thermophilus	taxID: 1308
	17	|	% 0.000
>Streptococcus sp. NPS 308	taxID: 1902136
	16	|	% 0.000
>Tenacibaculum sp. LPB0136	taxID: 1850252
	16	|	% 0.000
>Cupriavidus nantongensis	taxID: 1796606
	16	|	% 0.000
>Methylomonas sp. DH-1	taxID: 1727196
	16	|	% 0.000
>Massilia sp. NR 4-1	taxID: 1678028
	16	|	% 0.000
>Marinobacter sp. CP1	taxID: 1671721
	16	|	% 0.000
>Enterococcus phage EF62phi	taxID: 977801
	16	|	% 0.000
>Anaerostipes hadrus	taxID: 649756
	16	|	% 0.000
>Thiohalobacter thiocyanaticus	taxID: 585455
	16	|	% 0.000
>Sideroxydans lithotrophicus	taxID: 63745
	16	|	% 0.000
>Brevundimonas vesicularis	taxID: 41276
	16	|	% 0.000
>Staphylococcus xylosus	taxID: 1288
	16	|	% 0.000
>Legionella clemsonensis	taxID: 1867846
	15	|	% 0.000
>Microbulbifer aggregans	taxID: 1769779
	15	|	% 0.000
>Gemmatimonas phototrophica	taxID: 1379270
	15	|	% 0.000
>Thioalkalivibrio sulfidiphilus	taxID: 1033854
	15	|	% 0.000
>Pseudomonas xinjiangensis	taxID: 487184
	15	|	% 0.000
>Halotalea alkalilenta	taxID: 376489
	15	|	% 0.000
>Serratia rubidaea	taxID: 61652
	15	|	% 0.000
>Bacillus coagulans	taxID: 1398
	15	|	% 0.000
>Campylobacter ureolyticus	taxID: 827
	15	|	% 0.000
>Paracoccus denitrificans	taxID: 266
	15	|	% 0.000
>Rhizobium sp. TAL182	taxID: 2020313
	14	|	% 0.000
>Nostoc sp. NIES-4103	taxID: 2005458
	14	|	% 0.000
>Yersinia pseudotuberculosis complex	taxID: 1649845
	14	|	% 0.000
>Pseudomonas oryzae	taxID: 1392877
	14	|	% 0.000
>Desulfosporosinus acidiphilus	taxID: 885581
	14	|	% 0.000
>Methylobacterium extorquens group	taxID: 578822
	14	|	% 0.000
>Paenibacillus graminis	taxID: 189425
	14	|	% 0.000
>Oceanobacillus iheyensis	taxID: 182710
	14	|	% 0.000
>Delftia tsuruhatensis	taxID: 180282
	14	|	% 0.000
>Cupriavidus metallidurans	taxID: 119219
	14	|	% 0.000
>Roseateles depolymerans	taxID: 76731
	14	|	% 0.000
>Lactobacillus rhamnosus	taxID: 47715
	14	|	% 0.000
>Ferrimonas balearica	taxID: 44012
	14	|	% 0.000
>Lactobacillus salivarius	taxID: 1624
	14	|	% 0.000
>Lactobacillus gasseri	taxID: 1596
	14	|	% 0.000
>Enterococcus hirae	taxID: 1354
	14	|	% 0.000
>Streptomyces sp. CdTB01	taxID: 1725411
	13	|	% 0.000
>Bordetella sp. H567	taxID: 1697043
	13	|	% 0.000
>Bacillus gobiensis	taxID: 1441095
	13	|	% 0.000
>Bordetella genomosp. 9	taxID: 1416803
	13	|	% 0.000
>Pseudomonas sp. UW4	taxID: 1207075
	13	|	% 0.000
>Pseudomonas sp. StFLB209	taxID: 1028989
	13	|	% 0.000
>Salinicoccus halodurans	taxID: 407035
	13	|	% 0.000
>Arcobacter nitrofigilis	taxID: 28199
	13	|	% 0.000
>Alcaligenes faecalis	taxID: 511
	13	|	% 0.000
>Streptomyces sp. 3124.6	taxID: 1882757
	12	|	% 0.000
>Gemella sp. oral taxon 928	taxID: 1785995
	12	|	% 0.000
>Streptococcus sp. oral taxon 431	taxID: 712633
	12	|	% 0.000
>Lysobacter gummosus	taxID: 262324
	12	|	% 0.000
>Anoxybacillus gonensis	taxID: 198467
	12	|	% 0.000
>Chromohalobacter salexigens	taxID: 158080
	12	|	% 0.000
>Lachnoclostridium phytofermentans	taxID: 66219
	12	|	% 0.000
>Sphingobium chlorophenolicum	taxID: 46429
	12	|	% 0.000
>Capnocytophaga stomatis	taxID: 1848904
	11	|	% 0.000
>Citrobacter freundii complex	taxID: 1344959
	11	|	% 0.000
>Alcanivorax pacificus	taxID: 1306787
	11	|	% 0.000
>Advenella mimigardefordensis	taxID: 302406
	11	|	% 0.000
>Kosakonia cowanii	taxID: 208223
	11	|	% 0.000
>Laribacter hongkongensis	taxID: 168471
	11	|	% 0.000
>Magnetospirillum magneticum	taxID: 84159
	11	|	% 0.000
>Shewanella oneidensis	taxID: 70863
	11	|	% 0.000
>Shewanella baltica	taxID: 62322
	11	|	% 0.000
>Vibrio anguillarum	taxID: 55601
	11	|	% 0.000
>[Eubacterium] hallii	taxID: 39488
	11	|	% 0.000
>Faecalitalea cylindroides	taxID: 39483
	11	|	% 0.000
>Methanobacterium formicicum	taxID: 2162
	11	|	% 0.000
>Clostridium botulinum	taxID: 1491
	11	|	% 0.000
>Lysinibacillus sphaericus	taxID: 1421
	11	|	% 0.000
>Streptococcus gordonii	taxID: 1302
	11	|	% 0.000
>Desulfovibrio vulgaris	taxID: 881
	11	|	% 0.000
>Neisseria gonorrhoeae	taxID: 485
	11	|	% 0.000
>Azotobacter chroococcum	taxID: 353
	11	|	% 0.000
>Pseudomonas sp. M30-35	taxID: 1981174
	10	|	% 0.000
>Acidovorax sp. RAC01	taxID: 1842533
	10	|	% 0.000
>Enterobacter sp. FY-07	taxID: 1692238
	10	|	% 0.000
>Desulfurivibrio alkaliphilus	taxID: 427923
	10	|	% 0.000
>Natranaerobius thermophilus	taxID: 375929
	10	|	% 0.000
>Pandoraea apista	taxID: 93218
	10	|	% 0.000
>Saccharophagus degradans	taxID: 86304
	10	|	% 0.000
>Staphylococcus pasteuri	taxID: 45972
	10	|	% 0.000
>Cronobacter sakazakii	taxID: 28141
	10	|	% 0.000
>Corynebacterium flavescens	taxID: 28028
	10	|	% 0.000
>Bacillus cellulosilyticus	taxID: 1413
	10	|	% 0.000
>Pasteurella multocida	taxID: 747
	10	|	% 0.000
>Cupriavidus sp. NH9	taxID: 2036817
	9	|	% 0.000
>Rhodobacter sp. CZR27	taxID: 2033869
	9	|	% 0.000
>Labrenzia sp. VG12	taxID: 2021862
	9	|	% 0.000
>Bacillus glycinifermentans	taxID: 1664069
	9	|	% 0.000
>Mycobacterium sp. QIA-37	taxID: 1561223
	9	|	% 0.000
>Croceicoccus naphthovorans	taxID: 1348774
	9	|	% 0.000
>Chamaesiphon minutus	taxID: 1173032
	9	|	% 0.000
>Streptococcus sp. oral taxon 064	taxID: 712624
	9	|	% 0.000
>Rhodoplanes sp. Z2-YC6860	taxID: 674703
	9	|	% 0.000
>Bdellovibrio exovorus	taxID: 453816
	9	|	% 0.000
>Congregibacter litoralis	taxID: 393662
	9	|	% 0.000
>Candidatus Solibacter usitatus	taxID: 332163
	9	|	% 0.000
>Flavobacterium indicum	taxID: 312277
	9	|	% 0.000
>Pseudomonas rhizosphaerae	taxID: 216142
	9	|	% 0.000
>Acidaminococcus intestini	taxID: 187327
	9	|	% 0.000
>Paenibacillus terrae	taxID: 159743
	9	|	% 0.000
>Staphylococcus lugdunensis	taxID: 28035
	9	|	% 0.000
>Fervidobacterium nodosum	taxID: 2424
	9	|	% 0.000
>Acidipropionibacterium acidipropionici	taxID: 1748
	9	|	% 0.000
>Clostridium pasteurianum	taxID: 1501
	9	|	% 0.000
>Streptococcus parauberis	taxID: 1348
	9	|	% 0.000
>Pseudomonas sp. S-6-2	taxID: 1931241
	8	|	% 0.000
>Kurthia sp. 11kri321	taxID: 1750719
	8	|	% 0.000
>Sphingomonas hengshuiensis	taxID: 1609977
	8	|	% 0.000
>Leptotrichia sp. oral taxon 212	taxID: 712357
	8	|	% 0.000
>Streptomyces sp. Mg1	taxID: 465541
	8	|	% 0.000
>Pandoraea thiooxydans	taxID: 445709
	8	|	% 0.000
>Shewanella loihica	taxID: 359303
	8	|	% 0.000
>Riemerella anatipestifer	taxID: 34085
	8	|	% 0.000
>Tannerella forsythia	taxID: 28112
	8	|	% 0.000
>Desulfotomaculum ruminis	taxID: 1564
	8	|	% 0.000
>Virgibacillus halodenitrificans	taxID: 1482
	8	|	% 0.000
>Vitreoscilla filiformis	taxID: 63
	8	|	% 0.000
>Herbaspirillum sp. meg3	taxID: 2025949
	7	|	% 0.000
>Agarilytica rhodophyticola	taxID: 1737490
	7	|	% 0.000
>Fictibacillus phosphorivorans	taxID: 1221500
	7	|	% 0.000
>Burkholderia sp. RPE64	taxID: 758793
	7	|	% 0.000
>Bradyrhizobiaceae bacterium SG-6C	taxID: 709797
	7	|	% 0.000
>Streptococcus anginosus group	taxID: 671232
	7	|	% 0.000
>Pseudomonas cremoricolorata	taxID: 157783
	7	|	% 0.000
>Paraburkholderia fungorum	taxID: 134537
	7	|	% 0.000
>Bordetella trematum	taxID: 123899
	7	|	% 0.000
>Caldicellulosiruptor saccharolyticus	taxID: 44001
	7	|	% 0.000
>Photobacterium damselae	taxID: 38293
	7	|	% 0.000
>Halothiobacillus neapolitanus	taxID: 927
	7	|	% 0.000
>Vibrio vulnificus	taxID: 672
	7	|	% 0.000
>Lachnoclostridium sp. YL32	taxID: 1834196
	6	|	% 0.000
>Methylophilus sp. TWE2	taxID: 1662285
	6	|	% 0.000
>Sphingomonas taxi	taxID: 1549858
	6	|	% 0.000
>Idiomarinaceae bacterium HL-53	taxID: 1298881
	6	|	% 0.000
>Janthinobacterium svalbardensis	taxID: 368607
	6	|	% 0.000
>Sphingomonas koreensis	taxID: 93064
	6	|	% 0.000
>Anaerococcus mediterraneensis	taxID: 1870984
	5	|	% 0.000
>Neisseria sp. 10023	taxID: 1853278
	5	|	% 0.000
>Clostridium sp. SY8519	taxID: 1042156
	5	|	% 0.000
>Sphingomonas sp. KC8	taxID: 1030157
	5	|	% 0.000
>Streptomyces griseus group	taxID: 629295
	5	|	% 0.000
>Sphingopyxis sp. 113P3	taxID: 292913
	5	|	% 0.000
>Cupriavidus pinatubonensis	taxID: 248026
	5	|	% 0.000
>[Clostridium] bolteae	taxID: 208479
	5	|	% 0.000
>Staphylococcus pettenkoferi	taxID: 170573
	5	|	% 0.000
>Mycobacterium avium complex (MAC)	taxID: 120793
	5	|	% 0.000
>Streptococcus dysgalactiae group	taxID: 119603
	5	|	% 0.000
>Streptococcus cristatus	taxID: 45634
	5	|	% 0.000
>[Eubacterium] rectale	taxID: 39491
	5	|	% 0.000
>Desulfotomaculum nigrificans	taxID: 1565
	5	|	% 0.000
>Bacillus pumilus	taxID: 1408
	5	|	% 0.000
>Streptococcus parasanguinis	taxID: 1318
	5	|	% 0.000
>Edwardsiella tarda	taxID: 636
	5	|	% 0.000
>Brucella abortus	taxID: 235
	5	|	% 0.000
>Vibrio sp. Q67	taxID: 2025808
	4	|	% 0.000
>Marinilactibacillus sp. 15R	taxID: 1911586
	4	|	% 0.000
>Rhizobium sp. S41	taxID: 1869170
	4	|	% 0.000
>Fusobacterium hwasookii	taxID: 1583098
	4	|	% 0.000
>Sphingomonas sp. LK11	taxID: 1390395
	4	|	% 0.000
>Celeribacter indicus	taxID: 1208324
	4	|	% 0.000
>Providencia sneebia	taxID: 516075
	4	|	% 0.000
>Caulobacter sp. K31	taxID: 366602
	4	|	% 0.000
>Staphylococcus simiae	taxID: 308354
	4	|	% 0.000
>Sphingopyxis granuli	taxID: 267128
	4	|	% 0.000
>Halomonas campaniensis	taxID: 213554
	4	|	% 0.000
>Virgibacillus necropolis	taxID: 163877
	4	|	% 0.000
>Nocardia cyriacigeorgica	taxID: 135487
	4	|	% 0.000
>Erythrobacter litoralis	taxID: 39960
	4	|	% 0.000
>Desulfitobacterium dehalogenans	taxID: 36854
	4	|	% 0.000
>Vibrio nigripulchritudo	taxID: 28173
	4	|	% 0.000
>Porphyromonas asaccharolytica	taxID: 28123
	4	|	% 0.000
>Symbiobacterium thermophilum	taxID: 2734
	4	|	% 0.000
>Paenibacillus polymyxa	taxID: 1406
	4	|	% 0.000
>Streptococcus pyogenes	taxID: 1314
	4	|	% 0.000
>Streptococcus salivarius	taxID: 1304
	4	|	% 0.000
>Grimontia hollisae	taxID: 673
	4	|	% 0.000
>Francisella tularensis	taxID: 263
	4	|	% 0.000
>Lactococcus phage 98201	taxID: 1871690
	3	|	% 0.000
>Sphingobium sp. EP60837	taxID: 1855519
	3	|	% 0.000
>Ezakiella massiliensis	taxID: 1852374
	3	|	% 0.000
>Rhodobacter sp. LPB0142	taxID: 1850250
	3	|	% 0.000
>Streptococcus marmotae	taxID: 1825069
	3	|	% 0.000
>Christensenella massiliensis	taxID: 1805714
	3	|	% 0.000
>Yangia sp. CCB-MM3	taxID: 1792508
	3	|	% 0.000
>Enterococcus virus FL3	taxID: 1633151
	3	|	% 0.000
>Streptomyces leeuwenhoekii	taxID: 1437453
	3	|	% 0.000
>Sphingobium sp. TKS	taxID: 1315974
	3	|	% 0.000
>Staphylococcus agnetis	taxID: 985762
	3	|	% 0.000
>Janibacter indicus	taxID: 857417
	3	|	% 0.000
>Pseudomonas arsenicoxydans	taxID: 702115
	3	|	% 0.000
>Fusobacterium sp. oral taxon 203	taxID: 671211
	3	|	% 0.000
>Vagococcus teuberi	taxID: 519472
	3	|	% 0.000
>Halobacillus mangrovi	taxID: 402384
	3	|	% 0.000
>Micavibrio aeruginosavorus	taxID: 349221
	3	|	% 0.000
>Staphylococcus pseudintermedius	taxID: 283734
	3	|	% 0.000
>Corallococcus coralloides	taxID: 184914
	3	|	% 0.000
>Novosphingobium resinovorum	taxID: 158500
	3	|	% 0.000
>Mycobacterium goodii	taxID: 134601
	3	|	% 0.000
>Sphingobium cloacae	taxID: 120107
	3	|	% 0.000
>Lactobacillus mucosae	taxID: 97478
	3	|	% 0.000
>Maricaulis maris	taxID: 74318
	3	|	% 0.000
>Staphylococcus succinus	taxID: 61015
	3	|	% 0.000
>Enterococcus durans	taxID: 53345
	3	|	% 0.000
>Pediococcus damnosus	taxID: 51663
	3	|	% 0.000
>Nocardia nova	taxID: 37330
	3	|	% 0.000
>Odoribacter splanchnicus	taxID: 28118
	3	|	% 0.000
>Streptomyces violaceoruber	taxID: 1935
	3	|	% 0.000
>Streptomyces scabiei	taxID: 1930
	3	|	% 0.000
>Mycobacterium gilvum	taxID: 1804
	3	|	% 0.000
>Lactobacillus helveticus	taxID: 1587
	3	|	% 0.000
>Aerococcus viridans	taxID: 1377
	3	|	% 0.000
>Vibrio fluvialis	taxID: 676
	3	|	% 0.000
>Campylobacter fetus	taxID: 196
	3	|	% 0.000
>Halomonas sp. N3-2A	taxID: 2014541
	2	|	% 0.000
>Sphingomonas sp. NIC1	taxID: 1961362
	2	|	% 0.000
>Sphingomonas sp. LM7	taxID: 1938607
	2	|	% 0.000
>Mycobacterium sp. MS1601	taxID: 1936029
	2	|	% 0.000
>Streptococcus himalayensis	taxID: 1888195
	2	|	% 0.000
>Mycobacterium sp. djl-10	taxID: 1879023
	2	|	% 0.000
>Agrobacterium sp. RAC06	taxID: 1842536
	2	|	% 0.000
>Blastomonas sp. RAC04	taxID: 1842535
	2	|	% 0.000
>Bacillus sp. SDLI1	taxID: 1774743
	2	|	% 0.000
>Paenibacillus sp. LPB0068	taxID: 1763538
	2	|	% 0.000
>Streptococcus sp. A12	taxID: 1759399
	2	|	% 0.000
>Sphingobium sp. C1	taxID: 1673076
	2	|	% 0.000
>Paenibacillus physcomitrellae	taxID: 1619311
	2	|	% 0.000
>Jeotgalibacillus malaysiensis	taxID: 1508404
	2	|	% 0.000
>Geobacillus thermoleovorans group	taxID: 1505648
	2	|	% 0.000
>Martelella endophytica	taxID: 1486262
	2	|	% 0.000
>Sphingobium baderi	taxID: 1332080
	2	|	% 0.000
>Nitrospira japonica	taxID: 1325564
	2	|	% 0.000
>Thiobacimonas profunda	taxID: 1229727
	2	|	% 0.000
>Ruminococcus bicirculans	taxID: 1160721
	2	|	% 0.000
>Acinetobacter oleivorans	taxID: 1148157
	2	|	% 0.000
>Burkholderia sp. YI23	taxID: 1097668
	2	|	% 0.000
>Kibdelosporangium phytohabitans	taxID: 860235
	2	|	% 0.000
>Mycobacterium litorale	taxID: 758802
	2	|	% 0.000
>Clostridium sp. BNL1100	taxID: 755731
	2	|	% 0.000
>Sphingomonas sp. MM-1	taxID: 745310
	2	|	% 0.000
>Mycobacterium shigaense	taxID: 722731
	2	|	% 0.000
>Actinomyces sp. oral taxon 414	taxID: 712122
	2	|	% 0.000
>Altererythrobacter dongtanensis	taxID: 692370
	2	|	% 0.000
>Rhodanobacter denitrificans	taxID: 666685
	2	|	% 0.000
>Vagococcus penaei	taxID: 633807
	2	|	% 0.000
>Eggerthella sp. YY7918	taxID: 502558
	2	|	% 0.000
>Sphingobium sp. YBL2	taxID: 484429
	2	|	% 0.000
>Pseudomonas sabulinigri	taxID: 472181
	2	|	% 0.000
>Pantoea vagans	taxID: 470934
	2	|	% 0.000
>Croceicoccus marinus	taxID: 450378
	2	|	% 0.000
>Rhizobium sp. IRBG74	taxID: 424182
	2	|	% 0.000
>Paenibacillus donghaensis	taxID: 414771
	2	|	% 0.000
>Sphingobium sp. MI1205	taxID: 407020
	2	|	% 0.000
>Tessaracoccus flavescens	taxID: 399497
	2	|	% 0.000
>Rhodovulum sp. MB263	taxID: 308754
	2	|	% 0.000
>Lactobacillus kefiranofaciens	taxID: 267818
	2	|	% 0.000
>Chelativorans sp. BNC1	taxID: 266779
	2	|	% 0.000
>Dinoroseobacter shibae	taxID: 215813
	2	|	% 0.000
>Erwinia sp. Ejp617	taxID: 215689
	2	|	% 0.000
>Staphylococcus nepalensis	taxID: 214473
	2	|	% 0.000
>Mycobacterium sp. JS623	taxID: 212767
	2	|	% 0.000
>Parvularcula bermudensis	taxID: 208216
	2	|	% 0.000
>Thermodesulfatator indicus	taxID: 171695
	2	|	% 0.000
>Collimonas fungivorans	taxID: 158899
	2	|	% 0.000
>Desulfotalea psychrophila	taxID: 84980
	2	|	% 0.000
>Lactobacillus amylolyticus	taxID: 83683
	2	|	% 0.000
>Campylobacter hominis	taxID: 76517
	2	|	% 0.000
>Shewanella pealeana	taxID: 70864
	2	|	% 0.000
>Staphylococcus condimenti	taxID: 70255
	2	|	% 0.000
>Kitasatospora albolonga	taxID: 68173
	2	|	% 0.000
>Serratia ficaria	taxID: 61651
	2	|	% 0.000
>Mycoplasma crocodyli	taxID: 50052
	2	|	% 0.000
>Bacteroides caccae	taxID: 47678
	2	|	% 0.000
>Vibrio scophthalmi	taxID: 45658
	2	|	% 0.000
>Kutzneria albida	taxID: 43357
	2	|	% 0.000
>Streptomyces pristinaespiralis	taxID: 38300
	2	|	% 0.000
>Nocardia seriolae	taxID: 37332
	2	|	% 0.000
>Nocardia farcinica	taxID: 37329
	2	|	% 0.000
>Mycobacterium rhodesiae	taxID: 36814
	2	|	% 0.000
>Variovorax paradoxus	taxID: 34073
	2	|	% 0.000
>Bacillus cohnii	taxID: 33932
	2	|	% 0.000
>Mycobacterium haemophilum	taxID: 29311
	2	|	% 0.000
>Mycobacterium chubuense	taxID: 1800
	2	|	% 0.000
>Mycobacterium smegmatis	taxID: 1772
	2	|	% 0.000
>Mycobacterium kansasii	taxID: 1768
	2	|	% 0.000
>Clostridium novyi	taxID: 1542
	2	|	% 0.000
>Bacillus megaterium	taxID: 1404
	2	|	% 0.000
>Lactococcus raffinolactis	taxID: 1366
	2	|	% 0.000
>Streptococcus agalactiae	taxID: 1311
	2	|	% 0.000
>Streptococcus mutans	taxID: 1309
	2	|	% 0.000
>Streptococcus sanguinis	taxID: 1305
	2	|	% 0.000
>Staphylococcus simulans	taxID: 1286
	2	|	% 0.000
>Staphylococcus hyicus	taxID: 1284
	2	|	% 0.000
>Staphylococcus haemolyticus	taxID: 1283
	2	|	% 0.000
>Staphylococcus carnosus	taxID: 1281
	2	|	% 0.000
>Rhodopseudomonas palustris	taxID: 1076
	2	|	% 0.000
>Morganella morganii	taxID: 582
	2	|	% 0.000
>Klebsiella pneumoniae	taxID: 573
	2	|	% 0.000
>Pectobacterium carotovorum	taxID: 554
	2	|	% 0.000
>Bradyrhizobium sp.	taxID: 376
	2	|	% 0.000
>Stenotrophomonas sp. WZN-1	taxID: 2005046
	1	|	% 0.000
>Cellulomonas sp. PSBB021	taxID: 2003551
	1	|	% 0.000
>Paracoccus contaminans	taxID: 1945662
	1	|	% 0.000
>Capnocytophaga sp. H4358	taxID: 1945658
	1	|	% 0.000
>Shewanella sp. FDAARGOS_354	taxID: 1930557
	1	|	% 0.000
>Paraburkholderia sp. SOS3	taxID: 1926494
	1	|	% 0.000
>Sulfitobacter sp. AM1-D1	taxID: 1917485
	1	|	% 0.000
>Sphingopyxis sp. LPB0140	taxID: 1913578
	1	|	% 0.000
>Pseudomonas sp. BS-2016	taxID: 1907766
	1	|	% 0.000
>Tumebacillus sp. AR23208	taxID: 1903704
	1	|	% 0.000
>Paenibacillaceae bacterium GAS479	taxID: 1882832
	1	|	% 0.000
>Pseudomonas sp. 7SR1	taxID: 1881017
	1	|	% 0.000
>Ndongobacter massiliensis	taxID: 1871025
	1	|	% 0.000
>Macrococcus canis	taxID: 1855823
	1	|	% 0.000
>Arcobacter sp. LPB0137	taxID: 1850254
	1	|	% 0.000
>Rhizorhabdus dicambivorans	taxID: 1850238
	1	|	% 0.000
>Auricoccus indicus	taxID: 1849491
	1	|	% 0.000
>Bradyrhizobium sp. G22	taxID: 1839752
	1	|	% 0.000
>Lacunisphaera limnophila	taxID: 1838286
	1	|	% 0.000
>Streptococcus halotolerans	taxID: 1814128
	1	|	% 0.000
>Variovorax sp. PAMC 28711	taxID: 1795631
	1	|	% 0.000
>Bosea sp. PAMC 26642	taxID: 1792307
	1	|	% 0.000
>Flavobacterium crassostreae	taxID: 1763534
	1	|	% 0.000
>Devosia sp. A16	taxID: 1736675
	1	|	% 0.000
>Turicibacter sp. H121	taxID: 1712675
	1	|	% 0.000
>Bacillus sp. FJAT-18017	taxID: 1705566
	1	|	% 0.000
>Citrobacter sp. FDAARGOS_156	taxID: 1702170
	1	|	% 0.000
>Psychrobacter sp. P11F6	taxID: 1699621
	1	|	% 0.000
>Mycobacterium sp. YC-RL4	taxID: 1682113
	1	|	% 0.000
>Herbinix luporum	taxID: 1679721
	1	|	% 0.000
>Sulfurifustis variabilis	taxID: 1675686
	1	|	% 0.000
>Rhodococcus sp. PBTS 1	taxID: 1653478
	1	|	% 0.000
>Maribacter sp. 1_2014MBL_MicDiv	taxID: 1644130
	1	|	% 0.000
>Verrucomicrobia bacterium IMCC26134	taxID: 1637999
	1	|	% 0.000
>Citromicrobium sp. JL477	taxID: 1634516
	1	|	% 0.000
>Weissella jogaejeotgali	taxID: 1631871
	1	|	% 0.000
>Bacillus sp. LM 4-2	taxID: 1628753
	1	|	% 0.000
>Hoeflea sp. IMCC20628	taxID: 1620421
	1	|	% 0.000
>Acinetobacter sp. NCu2D-2	taxID: 1608473
	1	|	% 0.000
>Lentzea guizhouensis	taxID: 1586287
	1	|	% 0.000
>Haematospirillum jordaniae	taxID: 1549855
	1	|	% 0.000
>Mycobacterium sp. EPa45	taxID: 1545728
	1	|	% 0.000
>Paenibacillus sp. FSL R5-0345	taxID: 1536770
	1	|	% 0.000
>Brevundimonas sp. DS20	taxID: 1532555
	1	|	% 0.000
>Rickettsiales bacterium Ac37b	taxID: 1528098
	1	|	% 0.000
>Sphingopyxis fribergensis	taxID: 1515612
	1	|	% 0.000
>Methylobacterium sp. C1	taxID: 1479019
	1	|	% 0.000
>Zhongshania aliphaticivorans	taxID: 1470434
	1	|	% 0.000
>Paenibacillus yonginensis	taxID: 1462996
	1	|	% 0.000
>Vibrio tritonius	taxID: 1435069
	1	|	% 0.000
>Gloeobacter kilaueensis	taxID: 1416614
	1	|	% 0.000
>Xuhuaishuia manganoxidans	taxID: 1411903
	1	|	% 0.000
>Dill cryptic virus 1	taxID: 1408895
	1	|	% 0.000
>Dyella jiangningensis	taxID: 1379159
	1	|	% 0.000
>Aureimonas sp. AU20	taxID: 1349819
	1	|	% 0.000
>Defluviimonas alba	taxID: 1335048
	1	|	% 0.000
>Thermogutta terrifontis	taxID: 1331910
	1	|	% 0.000
>Flavobacterium communis	taxID: 1306519
	1	|	% 0.000
>Myxococcus hansupus	taxID: 1297742
	1	|	% 0.000
>Sphingomonas sp. DC-6	taxID: 1283312
	1	|	% 0.000
>Altererythrobacter atlanticus	taxID: 1267766
	1	|	% 0.000
>Pelagibaca abyssi	taxID: 1250539
	1	|	% 0.000
>Janthinobacterium sp. B9-8	taxID: 1236179
	1	|	% 0.000
>Geobacter daltonii	taxID: 1203471
	1	|	% 0.000
>Cloacibacillus porcorum	taxID: 1197717
	1	|	% 0.000
>Lactobacillus hokkaidonensis	taxID: 1193095
	1	|	% 0.000
>Agrobacterium tumefaciens complex	taxID: 1183400
	1	|	% 0.000
>Geitlerinema sp. PCC 7407	taxID: 1173025
	1	|	% 0.000
>Draconibacterium orientale	taxID: 1168034
	1	|	% 0.000
>Streptococcus sp. I-P16	taxID: 1156433
	1	|	% 0.000
>Moorea producens	taxID: 1155739
	1	|	% 0.000
>Massilia putida	taxID: 1141883
	1	|	% 0.000
>Thermus sp. CCB_US3_UF1	taxID: 1111069
	1	|	% 0.000
>Persicobacter sp. JZB09	taxID: 1085624
	1	|	% 0.000
>Bacillus virus G	taxID: 1084719
	1	|	% 0.000
>Streptomyces xinghaiensis	taxID: 1038928
	1	|	% 0.000
>Planococcus plakortidis	taxID: 1038856
	1	|	% 0.000
>Fimbriimonas ginsengisoli	taxID: 1005039
	1	|	% 0.000
>Paraburkholderia sprentiae	taxID: 948107
	1	|	% 0.000
>Sphingomonas indica	taxID: 941907
	1	|	% 0.000
>Yersinia entomophaga	taxID: 935293
	1	|	% 0.000
>Shinella sp. HZN7	taxID: 879274
	1	|	% 0.000
>Thermincola potens	taxID: 863643
	1	|	% 0.000
>Pseudomonas litoralis	taxID: 797277
	1	|	% 0.000
>cyanobacterium endosymbiont of Epithemia turgida	taxID: 718217
	1	|	% 0.000
>Frankia sp. QA3	taxID: 710111
	1	|	% 0.000
>Micromonospora zamorensis	taxID: 709883
	1	|	% 0.000
>Bacillus sp. 1NLA3E	taxID: 666686
	1	|	% 0.000
>Pandoraea vervacti	taxID: 656178
	1	|	% 0.000
>Methylocystis bryophila	taxID: 655015
	1	|	% 0.000
>Sulfuricella denitrificans	taxID: 649841
	1	|	% 0.000
>Sphingobium sp. SYK-6	taxID: 627192
	1	|	% 0.000
>Pantoea sp. At-9b	taxID: 592316
	1	|	% 0.000
>Terribacillus aidingensis	taxID: 586416
	1	|	% 0.000
>Dickeya sp. NCPPB 3274	taxID: 568766
	1	|	% 0.000
>Friedmanniella luteola	taxID: 546871
	1	|	% 0.000
>Altererythrobacter marensis	taxID: 543877
	1	|	% 0.000
>Pelagibacterium halotolerans	taxID: 531813
	1	|	% 0.000
>Paenibacillus xylanexedens	taxID: 528191
	1	|	% 0.000
>Halomonas chromatireducens	taxID: 507626
	1	|	% 0.000
>Deinococcus gobiensis	taxID: 502394
	1	|	% 0.000
>Myroides profundi	taxID: 480520
	1	|	% 0.000
>Altererythrobacter ishigakiensis	taxID: 476157
	1	|	% 0.000
>Ilumatobacter coccineus	taxID: 467094
	1	|	% 0.000
>Steroidobacter denitrificans	taxID: 465721
	1	|	% 0.000
>Alkaliphilus oremlandii	taxID: 461876
	1	|	% 0.000
>Geobacter sp. M18	taxID: 443143
	1	|	% 0.000
>Corynebacterium timonense	taxID: 441500
	1	|	% 0.000
>Methylobacterium phyllosphaerae	taxID: 418223
	1	|	% 0.000
>Enterococcus thailandicus	taxID: 417368
	1	|	% 0.000
>[Polyangium] brachysporum	taxID: 413882
	1	|	% 0.000
>Corynebacterium ureicelerivorans	taxID: 401472
	1	|	% 0.000
>Staphylococcus virus PH15	taxID: 399185
	1	|	% 0.000
>Thioalkalivibrio sp. K90mix	taxID: 396595
	1	|	% 0.000
>Mycobacterium rutilum	taxID: 370526
	1	|	% 0.000
>Herbaspirillum hiltneri	taxID: 341045
	1	|	% 0.000
>Nitrospira defluvii	taxID: 330214
	1	|	% 0.000
>Staphylococcus virus phiETA3	taxID: 326037
	1	|	% 0.000
>Dokdonella koreensis	taxID: 323415
	1	|	% 0.000
>Staphylococcus virus 69	taxID: 320834
	1	|	% 0.000
>Streptococcus gallolyticus	taxID: 315405
	1	|	% 0.000
>Pseudoalteromonas tunicata	taxID: 314281
	1	|	% 0.000
>Catenulispora acidiphila	taxID: 304895
	1	|	% 0.000
>Tateyamaria omphalii	taxID: 299262
	1	|	% 0.000
>Ruegeria sp. TM1040	taxID: 292414
	1	|	% 0.000
>Micromonospora rifamycinica	taxID: 291594
	1	|	% 0.000
>Segniliparus rotundus	taxID: 286802
	1	|	% 0.000
>Streptomyces rubrolavendulae	taxID: 285473
	1	|	% 0.000
>Methylobacterium aquaticum	taxID: 270351
	1	|	% 0.000
>Psychrobacter alimentarius	taxID: 261164
	1	|	% 0.000
>Fictibacillus arsenicus	taxID: 255247
	1	|	% 0.000
>Staphylococcus equorum	taxID: 246432
	1	|	% 0.000
>Pseudonocardia dioxanivorans	taxID: 240495
	1	|	% 0.000
>Methylobacterium populi	taxID: 223967
	1	|	% 0.000
>Polaromonas naphthalenivorans	taxID: 216465
	1	|	% 0.000
>Escherichia albertii	taxID: 208962
	1	|	% 0.000
>Novosphingobium pentaromativorans	taxID: 205844
	1	|	% 0.000
>Streptococcus pasteurianus	taxID: 197614
	1	|	% 0.000
>Vibrio coralliilyticus	taxID: 190893
	1	|	% 0.000
>Ralstonia insidiosa	taxID: 190721
	1	|	% 0.000
>Kribbella flavida	taxID: 182640
	1	|	% 0.000
>Corynebacterium riegelii	taxID: 156976
	1	|	% 0.000
>Staphylococcus lutrae	taxID: 155085
	1	|	% 0.000
>Streptococcus lutetiensis	taxID: 150055
	1	|	% 0.000
>Pseudomonas sp. LAB-08	taxID: 143813
	1	|	% 0.000
>[Eubacterium] sulci	taxID: 143393
	1	|	% 0.000
>Lactococcus phage ul36	taxID: 114416
	1	|	% 0.000
>Leptolyngbya sp. PCC 7376	taxID: 111781
	1	|	% 0.000
>Mycobacterium vanbaalenii	taxID: 110539
	1	|	% 0.000
>Corynebacterium falsenii	taxID: 108486
	1	|	% 0.000
>Ensifer adhaerens	taxID: 106592
	1	|	% 0.000
>Methylibium petroleiphilum	taxID: 105560
	1	|	% 0.000
>Ralstonia mannitolilytica	taxID: 105219
	1	|	% 0.000
>Bordetella hinzii	taxID: 103855
	1	|	% 0.000
>Streptococcus infantarius	taxID: 102684
	1	|	% 0.000
>Bacillus sp. OxB-1	taxID: 98228
	1	|	% 0.000
>Ruegeria pomeroyi	taxID: 89184
	1	|	% 0.000
>Caulobacter segnis	taxID: 88688
	1	|	% 0.000
>Serratia plymuthica	taxID: 82996
	1	|	% 0.000
>Pragia fontium	taxID: 82985
	1	|	% 0.000
>Delftia acidovorans	taxID: 80866
	1	|	% 0.000
>Cellulophaga baltica	taxID: 76594
	1	|	% 0.000
>Mesorhizobium amorphae	taxID: 71433
	1	|	% 0.000
>Pseudomonas sp. VLB120	taxID: 69328
	1	|	% 0.000
>Pantoea stewartii	taxID: 66269
	1	|	% 0.000
>Pluralibacter gergoviae	taxID: 61647
	1	|	% 0.000
>Lelliottia amnigena	taxID: 61646
	1	|	% 0.000
>Phaeobacter gallaeciensis	taxID: 60890
	1	|	% 0.000
>Actinotignum schaalii	taxID: 59505
	1	|	% 0.000
>Janthinobacterium agaricidamnosum	taxID: 55508
	1	|	% 0.000
>Sediminispirochaeta smaragdinae	taxID: 55206
	1	|	% 0.000
>Raoultella ornithinolytica	taxID: 54291
	1	|	% 0.000
>Lactobacillus lindneri	taxID: 53444
	1	|	% 0.000
>Intrasporangium calvum	taxID: 53358
	1	|	% 0.000
>Novosphingobium aromaticivorans	taxID: 48935
	1	|	% 0.000
>Desulfohalobium retbaense	taxID: 45663
	1	|	% 0.000
>Enterococcus cecorum	taxID: 44008
	1	|	% 0.000
>Tolumonas auensis	taxID: 43948
	1	|	% 0.000
>Butyrivibrio proteoclasticus	taxID: 43305
	1	|	% 0.000
>Streptomyces collinus	taxID: 42684
	1	|	% 0.000
>Xenorhabdus bovienii	taxID: 40576
	1	|	% 0.000
>Dialister pneumosintes	taxID: 39950
	1	|	% 0.000
>[Eubacterium] eligens	taxID: 39485
	1	|	% 0.000
>Acholeplasma palmae	taxID: 38986
	1	|	% 0.000
>Corynebacterium mycetoides	taxID: 38302
	1	|	% 0.000
>Enterococcus casseliflavus	taxID: 37734
	1	|	% 0.000
>Nocardia brasiliensis	taxID: 37326
	1	|	% 0.000
>Citrobacter amalonaticus	taxID: 35703
	1	|	% 0.000
>Paracoccus aminophilus	taxID: 34003
	1	|	% 0.000
>Sphingopyxis macrogoltabida	taxID: 33050
	1	|	% 0.000
>Achromobacter denitrificans	taxID: 32002
	1	|	% 0.000
>Vibrio furnissii	taxID: 29494
	1	|	% 0.000
>Clostridium argentinense	taxID: 29341
	1	|	% 0.000
>Anaerotignum propionicum	taxID: 28446
	1	|	% 0.000
>Serratia proteamaculans	taxID: 28151
	1	|	% 0.000
>Prevotella intermedia	taxID: 28131
	1	|	% 0.000
>Burkholderia gladioli	taxID: 28095
	1	|	% 0.000
>Haliscomenobacter hydrossis	taxID: 2350
	1	|	% 0.000
>Methanosarcina mazei	taxID: 2209
	1	|	% 0.000
>Ureaplasma urealyticum	taxID: 2130
	1	|	% 0.000
>Kitasatospora setae	taxID: 2066
	1	|	% 0.000
>Streptosporangium roseum	taxID: 2001
	1	|	% 0.000
>Streptomyces globisporus	taxID: 1908
	1	|	% 0.000
>Streptomyces anulatus	taxID: 1892
	1	|	% 0.000
>Mycobacterium vaccae	taxID: 1810
	1	|	% 0.000
>Mycobacterium thermoresistibile	taxID: 1797
	1	|	% 0.000
>Mycobacterium aurum	taxID: 1791
	1	|	% 0.000
>Mycobacterium simiae	taxID: 1784
	1	|	% 0.000
>Peptoclostridium acidaminophilum	taxID: 1731
	1	|	% 0.000
>Trueperella pyogenes	taxID: 1661
	1	|	% 0.000
>Listeria seeligeri	taxID: 1640
	1	|	% 0.000
>Lactobacillus ruminis	taxID: 1623
	1	|	% 0.000
>Lactobacillus pentosus	taxID: 1589
	1	|	% 0.000
>Lactobacillus brevis	taxID: 1580
	1	|	% 0.000
>Sporosarcina ureae	taxID: 1571
	1	|	% 0.000
>[Clostridium] cellulolyticum	taxID: 1521
	1	|	% 0.000
>Clostridium perfringens	taxID: 1502
	1	|	% 0.000
>Clostridium cellulovorans	taxID: 1493
	1	|	% 0.000
>Clostridium butyricum	taxID: 1492
	1	|	% 0.000
>Sporosarcina psychrophila	taxID: 1476
	1	|	% 0.000
>Amphibacillus xylanus	taxID: 1449
	1	|	% 0.000
>Planococcus kocurii	taxID: 1374
	1	|	% 0.000
>Lactococcus garvieae	taxID: 1363
	1	|	% 0.000
>Staphylococcus schleiferi	taxID: 1295
	1	|	% 0.000
>Staphylococcus muscae	taxID: 1294
	1	|	% 0.000
>Staphylococcus warneri	taxID: 1292
	1	|	% 0.000
>Rhodospirillum rubrum	taxID: 1085
	1	|	% 0.000
>Pararhodospirillum photometricum	taxID: 1084
	1	|	% 0.000
>Blastochloris viridis	taxID: 1079
	1	|	% 0.000
>Nitrobacter winogradskyi	taxID: 913
	1	|	% 0.000
>Nitrobacter hamburgensis	taxID: 912
	1	|	% 0.000
>Desulfovibrio africanus	taxID: 873
	1	|	% 0.000
>Prevotella ruminicola	taxID: 839
	1	|	% 0.000
>Aliivibrio fischeri	taxID: 668
	1	|	% 0.000
>Yersinia enterocolitica	taxID: 630
	1	|	% 0.000
>Shigella dysenteriae	taxID: 622
	1	|	% 0.000
>Hafnia alvei	taxID: 569
	1	|	% 0.000
>Neisseria meningitidis	taxID: 487
	1	|	% 0.000
>Neisseria lactamica	taxID: 486
	1	|	% 0.000
>Rhizobium leguminosarum	taxID: 384
	1	|	% 0.000
>Sinorhizobium fredii	taxID: 380
	1	|	% 0.000
>Bradyrhizobium japonicum	taxID: 375
	1	|	% 0.000
>Ralstonia pickettii	taxID: 329
	1	|	% 0.000
>Thermus aquaticus	taxID: 271
	1	|	% 0.000
>Helicobacter pylori	taxID: 210
	1	|	% 0.000
>Azospirillum lipoferum	taxID: 193
	1	|	% 0.000
>Leptospira interrogans	taxID: 173
	1	|	% 0.000
>Myxococcus macrosporus	taxID: 35
	1	|	% 0.000
>Shewanella putrefaciens	taxID: 24
	1	|	% 0.000
>Azorhizobium caulinodans	taxID: 7
	1	|	% 0.000



Composizione dataset input secondo headers stringhe

>A_hydrophila_HiSeq
	1000000	|		% 10.000
>B_cereus_HiSeq
	1000000	|		% 10.000
>B_fragilis_HiSeq
	1000000	|		% 10.000
>M_abscessus_HiSeq
	1000000	|		% 10.000
>P_fermentans_HiSeq
	1000000	|		% 10.000
>R_sphaeroides_HiSeq
	1000000	|		% 10.000
>S_aureus_HiSeq
	1000000	|		% 10.000
>S_pneumoniae_HiSeq
	1000000	|		% 10.000
>V_cholerae_HiSeq
	1000000	|		% 10.000
>X_axonopodis_HiSeq
	1000000	|		% 10.000


/*
 * Copyright 2013-2015, Derrick Wood <dwood@cs.jhu.edu>
 *
 * This file is part of the Kraken taxonomic sequence classification system.
 *
 * Kraken is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kraken is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kraken.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "kraken_headers.hpp"
#include "krakendb.hpp"
#include "krakenutil.hpp"
#include "quickfile.hpp"
#include "seqreader.hpp"
#include <iomanip>


#include <iostream>
#include <map>
#include <set>
#include <algorithm>
#include <functional>

const size_t DEF_WORK_UNIT_SIZE = 500000;

using namespace std;
using namespace kraken;

void parse_command_line(int argc, char **argv);
void usage(int exit_code=EX_USAGE);

/* Metodo per leggere e sequenziare la stringa di dna
 * glob_hit_counts -> chiave = id tassonomia, valore = numero di k-mer associati all'id della tassonomia per tutte le reads
 * tot_kmers -> insieme dei k-mer per tutte le reads
 * tot_kmers_cl -> k-mer classificati tra tutti quelli creati, per tutte le reads
 * inputmap -> mappa con conteggi degli organismi presenti in input in base all'header del fasta file
 */
void process_file(char *filename,
                  //output
                  map<uint32_t, uint32_t> &glob_hit_counts,
                  uint64_t &tot_kmers,
                  uint64_t &tot_kmers_cl,
                  map<string, uint32_t> &inputmap);

/* Metodo che esegue la classificazione delle sequenze
 * koss -> kraken ostringstream
 * coss -> classified ostringstream
 * ucoss -> unclassified ostringstream
 * glob_hit_count -> mappa con i conteggi globali dei k-mers per la costruzione dell'albero globale
 * tot_kmer -> k-mers totali sequenziati
 * tot_kmer_cl -> numero totale di k-mers classificati tra quelli trovati
 * input_map -> mappa con conteggi degli organismi presenti in input in base all'header del fasta file
 */
void classify_sequence(DNASequence &dna, ostringstream &koss,
                       ostringstream &coss, ostringstream &uoss,
                       ostringstream &coss2, ostringstream &uoss2,
                       //output
                       map<uint32_t, uint32_t> &glob_hit_count,
                       uint64_t &tot_kmer,
                       uint64_t &tot_kmer_cl,
                       map<string, uint32_t> &input_map);

/* Metodo per estrarre i nodi a livello specie
 * g_h_c -> mappa dei conteggi globali
 * R_m -> mappa dei rank di ogni nodo
 * Par_ma -> parent map
 * C_m -> mappa dei figli di ciascun nodo
 * S_m -> mappa in output con i nodi a livello specie e come valore il numero dei kmer di quel nodo più quelli delle sue sottospecie
 * tot_kmer_s -> valore intero in uscita con tutti i kmer classificati a livello specie
 */
void extract_species(map<uint32_t, uint32_t> &g_h_c,
                     map<uint32_t, uint8_t> &R_m,
                     map<uint32_t, uint32_t> Par_ma,
                     multimap<uint32_t, uint32_t> &C_m,
                     //output
                     map<uint32_t, uint32_t> &S_m,
                     uint64_t &tot_kmer_s);

/*
 * Metodo che ritorna mappa dei nodi a livello specie con score la somma dei k-mers sul path da quel nodo alla radice
 * glob_h_cou -> mappa dei conteggi globali
 * map_spec -> mappa con i nodi a livello specie ed il numero finale dei kmer di quel nodo
 * pare_map -> parent map per la relazione padre-figlio
 * path_m -> mappa con somma dei k-mers sul path da quel nodo alla radice
 * tot_kmer_s_path -> numero totale di kmers sul percorso radice-foglia
 */
void taxonomy_tree_path(map<uint32_t, uint32_t> &glob_h_cou,
                        map<uint32_t, uint32_t> &map_spec,
                        map<uint32_t, uint32_t> &pare_map,
                        //output
                        map<uint32_t, uint32_t> &path_m,
                        uint64_t &tot_kmer_s_path);
/*
 *Metodo che ritorna i risultati
 * S_N_m -> mappa che associa ad ogni nodo specie ilnome scientifico della specie individuata
 * s_m -> mappa dell'analisi a livello specie
 * t_k_s -> contatore globale con i k-mers solo a livello specie
 * p_s_map -> mappa dell'analisi a livello path
 * t_k_s_path -> contatore globale con i k-mers asspciati al path di ogni singolo nodo a livello specie verso la radice
 */
string species_level_output(map<uint32_t, string> &S_N_m,
                            map<uint32_t, uint32_t> &s_m ,
                            uint64_t &t_k_s,
                            map<uint32_t, uint32_t> p_s_map,
                            uint64_t t_k_s_path);
/*
 ******************
 * metodi di Kraken
 *******************
 */
string hitlist_string(vector<uint32_t> &taxa, vector<uint8_t> &ambig);
set<uint32_t> get_ancestry(uint32_t taxon);
void report_stats(struct timeval time1, struct timeval time2);

/*
 ***********************
 * variabili di Kraken
 ***********************
 */
int Num_threads = 1;
string DB_filename, Index_filename, Nodes_filename, Names_filename;
bool Quick_mode = false;
bool Fastq_input = false;
bool Fastq_output = false;
bool Paired_input = false;
bool Print_classified = false;
bool Print_unclassified = false;
bool Print_kraken = true;
bool Populate_memory = false;
bool Only_classified_kraken_output = false;
uint32_t Minimum_hit_count = 1;
map<uint32_t, uint32_t> Parent_map;
KrakenDB Database;
string Classified_output_file, Unclassified_output_file, Kraken_output_file;
string Output_format;
ostream *Classified_output;
ostream *Classified_output2;
ostream *Unclassified_output;
ostream *Unclassified_output2;
ostream *Kraken_output;
size_t Work_unit_size = DEF_WORK_UNIT_SIZE;

uint64_t total_classified = 0;
uint64_t total_sequences = 0;
uint64_t total_bases = 0;

/*
 ******************************************
 * variabili nostre per tassonomia globale
 ******************************************
 */
uint64_t total_kmers=0;
uint64_t total_kmers_class=0;
uint64_t total_kmer_species=0;
uint64_t total_kmer_species_path=0;
uint64_t total_kmer_genus_species_path=0;

/*
 **************************************
 * mappe nostre per tassonomia globale
 **************************************
 */
map<uint32_t, uint32_t> global_hit_counts;
map<uint32_t, uint32_t> species_map;
map<uint32_t, uint32_t> path_species_map;
map<string, uint32_t> file_input_map;
map<uint32_t, uint32_t> path_genus_species_map;
map<uint32_t, uint8_t> Rank_map;
map<uint32_t, string> Scient_Names_map;
multimap<uint32_t, uint32_t> Children_map;


int main(int argc, char **argv) {
  #ifdef _OPENMP
  omp_set_num_threads(1);
  #endif

  parse_command_line(argc, argv);
  if (! Nodes_filename.empty()){

      /*
       *  metodo importato da krakenutil, crea la mappa con chiave il nodo e come valore il genitore di quel nodo
       * (mi serve per conoscere gli archi verso l'alto quando salgo l'albero verso la radice)
       */
      Parent_map = build_parent_map(Nodes_filename);

      Rank_map = build_rank_map(Nodes_filename); // metodo nostro: crea la mappa con i rank di ogni nodo secondo il DB in input (da krakenutil)

  }

  if(! Names_filename.empty())
      Scient_Names_map = build_names_map(Names_filename); // metodo nostro: crea la mappa con i nomi scientifici di ogni nodo secondo il DB

  // caricamento del DB su RAM
  if (Populate_memory)
      cerr << "Loading database... ";

  QuickFile db_file;
  db_file.open_file(DB_filename); // apre il file con il database
  if (Populate_memory)
      db_file.load_file(); // carica il file con il database
  Database = KrakenDB(db_file.ptr()); // inizializza un puntatore per scorrere le celle del database
  KmerScanner::set_k(Database.get_k());

  // fa lo stesso con gli indici delle posizioni dei kmer nella tabella
  QuickFile idx_file;
  idx_file.open_file(Index_filename);
  if (Populate_memory)
      idx_file.load_file();
  KrakenDBIndex db_index(idx_file.ptr());
  Database.set_index(&db_index);

  if (Populate_memory)
      cerr << "complete." << endl;

  /*
   * output standard di kraken
   *
   * NON MODIFICATI
   *
   */
  if (Print_classified) {
      if (Classified_output_file == "-")
          Classified_output = &cout;
      else {
          if (Output_format == "paired" && Fastq_output && ! Classified_output_file.empty()) {
              string Classified_output_filename1 = Classified_output_file + "_R1.fastq";
              string Classified_output_filename2 = Classified_output_file + "_R2.fastq";
              Classified_output  = new ofstream(Classified_output_filename1.c_str());
              Classified_output2 = new ofstream(Classified_output_filename2.c_str());
          }
          else if (Output_format == "paired" && ! Fastq_output && ! Classified_output_file.empty()) {
              string Classified_output_filename1 = Classified_output_file + "_R1.fa";
              string Classified_output_filename2 = Classified_output_file + "_R2.fa";
              Classified_output  = new ofstream(Classified_output_filename1.c_str());
              Classified_output2 = new ofstream(Classified_output_filename2.c_str());
          }
          else {
              Classified_output = new ofstream(Classified_output_file.c_str());
              Classified_output2 = new ofstream();
          }
      }
  }

  if (Print_unclassified) {
      if (Unclassified_output_file == "-")
          Unclassified_output = &cout;
      else {
          if (Output_format == "paired" && Fastq_output && ! Unclassified_output_file.empty()) {
              string Unclassified_output_filename1 = Unclassified_output_file + "_R1.fastq";
              string Unclassified_output_filename2 = Unclassified_output_file + "_R2.fastq";
              Unclassified_output  = new ofstream(Unclassified_output_filename1.c_str());
              Unclassified_output2 = new ofstream(Unclassified_output_filename2.c_str());
          }
          else if (Output_format == "paired" && ! Fastq_output && ! Unclassified_output_file.empty()) {
              string Unclassified_output_filename1 = Unclassified_output_file + "_R1.fa";
              string Unclassified_output_filename2 = Unclassified_output_file + "_R2.fa";
              Unclassified_output  = new ofstream(Unclassified_output_filename1.c_str());
              Unclassified_output2 = new ofstream(Unclassified_output_filename2.c_str());
          }
          else {
              Unclassified_output = new ofstream(Unclassified_output_file.c_str());
              Unclassified_output2 = new ofstream();
          }
      }
  }

  if (! Kraken_output_file.empty()) {
      if (Kraken_output_file == "-")
          Print_kraken = false;
      else
          Kraken_output = new ofstream(Kraken_output_file.c_str());
  }
  else
      Kraken_output = &cout;
/*
 * FINE PARTE NON MODIFICATA
 */

  // tempi di elaborazione dei dati
  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);
  // processo tutti i file in input con kraken
  for (int i = optind; i < argc; i++)

      /*
       *******************************
       * INIZIO PARTE MODIFICA NOSTRA
       *******************************
       */
      process_file(argv[i], global_hit_counts, total_kmers, total_kmers_class, file_input_map);

  // file di output post popolamento albero
  std::ofstream file;
  file.open("kmer_classify.txt", std::ios::out);

  Children_map = build_children_multimap(Parent_map); // metodo nostro: crea la multimappa con i figli per ogni nodo a livello specie e
                                                      // sottospecie

  // uscita albero taxonomy globale
  file << "\n" << "Classificazione kmers" << "\n" << "\n";
  file << "Totale kmers trovati nel file in input: " << total_kmers << "\n";
  file << "Kmers classificati: " << total_kmers_class << " (" << (total_kmers_class * 100.0)/total_kmers << " %" << " )\n";
  file << "Kmers non classificati: " << total_kmers - total_kmers_class << " (" << ((total_kmers - total_kmers_class) * 100.0)/total_kmers << " %" << " )\n\n";

  // costruisco la mappa dei nodi a livello specie con score il numero di kmer effettivi trovati nel nodo
  extract_species(global_hit_counts,Rank_map,Parent_map,Children_map,species_map,total_kmer_species);

  // costruisco la mappa dei nodi a livello specie con score il valore somma kmer dei path dal nodo alla radice
  taxonomy_tree_path(global_hit_counts,species_map, Parent_map, path_species_map, total_kmer_species_path);

  file << "Kmers classificati livello specie: " << total_kmer_species << " (" << (total_kmer_species * 100.0)/total_kmers_class << " %" << " )";

  //file << "\n"<< "Kmers del paths: "<< total_kmer_species_path;
  file << "\n\n";

  // output calssificazione kmer
  file <<  species_level_output(Scient_Names_map, species_map, total_kmer_species, path_species_map, total_kmer_species_path);

  file << "\n\n\n";
  file << "Composizione dataset input secondo headers stringhe" << "\n\n";

  // output cosa c'è nel file di input secondo le stringhe
  map<string,uint32_t>::iterator inp = file_input_map.begin();
  uint32_t somma = file_input_map["Somma"];
  file_input_map.erase("Somma");
  // scorro la mappa
  while(inp != file_input_map.end()){
      string testa = inp -> first;
      file << ">" << testa << "\n" << "\t" << file_input_map[testa] << "\t" << "|" << "\t" << "\t% " << setiosflags(ios::fixed)
           << setprecision (3) << file_input_map[testa] * 100.0/somma << "\n";
      inp++;
  }

  file <<  endl;
  file.close();

  gettimeofday(&tv2, NULL);
  report_stats(tv1, tv2);
  return 0;
}

string species_level_output(map<uint32_t, string> &S_N_m, map<uint32_t, uint32_t> &s_m , uint64_t &t_k_s,
                            map<uint32_t, uint32_t> p_s_map, uint64_t t_k_s_path){

    ostringstream specieslist;
    // visualizzo in ordine decrescente di percentuale
    multimap<uint32_t,uint32_t> ordine = build_children_multimap(s_m);
    // scorro la mappa
    std::multimap<uint32_t,uint32_t>::reverse_iterator it;
      for (it=ordine.rbegin(); it!=ordine.rend(); ++it){

        uint32_t taxon = it->second;
        uint32_t node = taxon;

        // struttura del file di output
        specieslist << ">" << S_N_m[node] << "\t" << "taxID: " << node << "\n";
        specieslist << "\t" << s_m[node] << "\t" << "|" << "\t" << "% " << setiosflags(ios::fixed)
                    << setprecision (3)  << (((uint64_t)s_m[node]*100.0))/t_k_s;

        /*
         * decommentare per ottenere in output anche le percentuali propagando il path
         */
        //specieslist << "\t" << "|" << "\t" << p_s_map[node] << "\t" << "|" << "\t" << "% " << setiosflags(ios::fixed)
        //            << setprecision (3) << (((uint64_t)p_s_map[node]*100.0))/t_k_s_path;
        specieslist << "\n";
    }

    return specieslist.str();
}

void extract_species(map<uint32_t, uint32_t> &g_h_c,
                     map<uint32_t, uint8_t> &R_m,
                     map<uint32_t, uint32_t> Par_ma,
                     multimap<uint32_t, uint32_t> &C_m,
                     //output
                     map<uint32_t, uint32_t> &S_m,
                     uint64_t &tot_kmer_s){

    map<uint32_t, uint8_t>::iterator it =R_m.begin();
    while(it !=R_m.end()){ // itero sulla mappa globale dell'albero con tutti i conteggi dei kmer
        uint32_t taxon = it->first; // estraggo un nodo
        uint32_t node = taxon;

        if(R_m[node]==7 && ((R_m[Par_ma[node]]<7) || (R_m[Par_ma[node]]>7))){ // verifico che il nodo sia un nodo specie (spe=7)
            uint32_t value =g_h_c[node]; //n° kmer in quel nodo

            // creo iteratore sui nodi figli del nodo specie (NODI SOTTOSPECIE)
            pair <multimap<uint32_t, uint32_t>::iterator, multimap<uint32_t, uint32_t>::iterator> ret;
            ret = C_m.equal_range(node);

            for (multimap<uint32_t, uint32_t>::iterator ch=ret.first; ch!=ret.second; ++ch){ // itero sui nodi sottospecie (figli)
                  uint32_t child = ch->second;
                  value=value+g_h_c[child]; // sommo al n° dei kmer del nodo specie il n° dei kmer delle sottospecie

                  pair <multimap<uint32_t, uint32_t>::iterator, multimap<uint32_t, uint32_t>::iterator> ter;
                  ter = C_m.equal_range(child);
                  for (multimap<uint32_t, uint32_t>::iterator cch=ter.first; cch!=ter.second; ++cch){ // itero sui nodi sottospecie (figli)
                        uint32_t chil = cch->second;
                        value=value+g_h_c[chil]; // sommo al n° dei kmer del nodo specie il n° dei kmer delle sottospecie

                        pair <multimap<uint32_t, uint32_t>::iterator, multimap<uint32_t, uint32_t>::iterator> awa;
                        awa = C_m.equal_range(chil);
                        for (multimap<uint32_t, uint32_t>::iterator ccch=awa.first; ccch!=awa.second; ++ccch){ // itero sui nodi sottospecie (figli)
                              uint32_t chils = ccch->second;
                              value=value+g_h_c[chils]; // sommo al n° dei kmer del nodo specie il n° dei kmer delle sottospecie

                              pair <multimap<uint32_t, uint32_t>::iterator, multimap<uint32_t, uint32_t>::iterator> quattro;
                              quattro = C_m.equal_range(chils);
                              for (multimap<uint32_t, uint32_t>::iterator cscch=quattro.first; cscch!=quattro.second; ++cscch){ // itero sui nodi sottospecie (figli)
                                    uint32_t qua = cscch->second;
                                    value=value+g_h_c[qua]; // sommo al n° dei kmer del nodo specie il n° dei kmer delle sottospecie

                                    pair <multimap<uint32_t, uint32_t>::iterator, multimap<uint32_t, uint32_t>::iterator> cinque;
                                    cinque = C_m.equal_range(chils);
                                    for (multimap<uint32_t, uint32_t>::iterator cscchs=cinque.first; cscchs!=cinque.second; ++cscchs){ // itero sui nodi sottospecie (figli)
                                          uint32_t cin = cscchs->second;
                                          value=value+g_h_c[cin]; // sommo al n° dei kmer del nodo specie il n° dei kmer delle sottospecie
                                    }
                              }
                        }
                  }
            }

            if(value>0){ // controllo che se quel nodo specie ha conteggio zero dopo aver accorpato i figli non lo metto in mappa
                S_m[node]=value; // creo mappa chiave->taxID / valore-> n°kmer a livello specie
                tot_kmer_s=tot_kmer_s+value; // contatore numero di kmer totali a livello specie
            }
        }

        it++;
    }
}

void taxonomy_tree_path(map<uint32_t, uint32_t> &glob_h_cou,
                        map<uint32_t, uint32_t> &map_spec,
                        map<uint32_t, uint32_t> &pare_map,
                        //output
                        map<uint32_t, uint32_t> &path_m,
                        uint64_t &tot_kmer_s_path){

    map<uint32_t, uint32_t>::iterator it = map_spec.begin();

    // Sum each taxon's LTR path
    while (it != map_spec.end()) {
        uint32_t taxon = it->first;
        uint32_t curr_node=taxon;
        uint32_t node = pare_map[curr_node];
        uint32_t score = map_spec[curr_node];
        while (node > 0) {
            score = score + glob_h_cou[node];
            node = pare_map[node];
        }
        path_m[curr_node] = score;
        tot_kmer_s_path = tot_kmer_s_path + score;
        it++;
    }
}

/*
 *******************************
 * FINE PARTE MODIFICA NOSTRA
 *******************************
 */

// non modificato
void report_stats(struct timeval time1, struct timeval time2) {
  time2.tv_usec -= time1.tv_usec;
  time2.tv_sec -= time1.tv_sec;
  if (time2.tv_usec < 0) {
    time2.tv_sec--;
    time2.tv_usec += 1000000;
  }
  double seconds = time2.tv_usec;
  seconds /= 1e6;
  seconds += time2.tv_sec;

  if (isatty(fileno(stderr)))
    cerr << "\r";
  fprintf(stderr, 
          "%llu sequences (%.2f Mbp) processed in %.3fs (%.1f Kseq/m, %.2f Mbp/m).\n",
          (unsigned long long) total_sequences, total_bases / 1.0e6, seconds,
          total_sequences / 1.0e3 / (seconds / 60),
          total_bases / 1.0e6 / (seconds / 60) );
  fprintf(stderr, "  %llu sequences classified (%.2f%%)\n",
          (unsigned long long) total_classified, total_classified * 100.0 / total_sequences);
  fprintf(stderr, "  %llu sequences unclassified (%.2f%%)\n",
          (unsigned long long) (total_sequences - total_classified),
          (total_sequences - total_classified) * 100.0 / total_sequences);
}

// MODIFICATO
void process_file(char *filename,
                  map<uint32_t, uint32_t> &glob_hit_counts,
                  uint64_t &tot_kmers,
                  uint64_t &tot_kmers_cl,
                 map<string, uint32_t> &inputmap) {

  string file_str(filename);
  DNASequenceReader *reader;
  DNASequence dna;

  if (Fastq_input)
    reader = new FastqReader(file_str);
  else
    reader = new FastaReader(file_str);

  #pragma omp parallel
  {
    vector<DNASequence> work_unit;
    ostringstream kraken_output_ss, classified_output_ss, classified_output_ss2, unclassified_output_ss, unclassified_output_ss2;

    while (reader->is_valid()) {
      work_unit.clear();
      size_t total_nt = 0;
      #pragma omp critical(get_input)
      {
        while (total_nt < Work_unit_size) {
          dna = reader->next_sequence();
          if (! reader->is_valid())
            break;
          work_unit.push_back(dna);
          total_nt += dna.seq.size();
        }
      }
      if (total_nt == 0)
        break;
      
      kraken_output_ss.str("");
      classified_output_ss.str("");
      classified_output_ss2.str("");
      unclassified_output_ss.str("");
      unclassified_output_ss2.str("");
      for (size_t j = 0; j < work_unit.size(); j++)

         // METODO CLASSIFICAZIONE MODIFICATO
        classify_sequence( work_unit[j],
                           kraken_output_ss,
                           classified_output_ss,
                           unclassified_output_ss,
                           classified_output_ss2,
                           unclassified_output_ss2,
                           glob_hit_counts,
                           tot_kmers,
                           tot_kmers_cl,
                           inputmap);

      /*
       * non modificato
       */
      #pragma omp critical(write_output)
      {
        if (Print_kraken)
          (*Kraken_output) << kraken_output_ss.str();
        if (Print_classified) {
          (*Classified_output) << classified_output_ss.str();
	  if (Output_format == "paired")
	    (*Classified_output2) << classified_output_ss2.str();
	}
        if (Print_unclassified) {
          (*Unclassified_output) << unclassified_output_ss.str();
	  if (Output_format == "paired")
	    (*Unclassified_output2) << unclassified_output_ss2.str();
	}
        total_sequences += work_unit.size();
        total_bases += total_nt;
        if (isatty(fileno(stderr)))
          cerr << "\rProcessed " << total_sequences << " sequences (" << total_bases << " bp) ...";
      }
    }
  }  // end parallel section

  delete reader;
  if (Print_kraken)
    (*Kraken_output) << std::flush;
  if (Print_classified) {
    (*Classified_output) << std::flush;
    (*Classified_output2) << std::flush;
   }
  if (Print_unclassified) {
    (*Unclassified_output) << std::flush;
    (*Unclassified_output2) << std::flush;
  }
}

// METODO CLASSIFICAZIONE MODIFICATO
void classify_sequence(DNASequence &dna,
                       ostringstream &koss,
                       ostringstream &coss,
                       ostringstream &uoss,
                       ostringstream &coss2,
                       ostringstream &uoss2,
                       map<uint32_t, uint32_t> &glob_hit_count,
                       uint64_t &tot_kmer,
                       uint64_t &tot_kmer_cl,
                       map<string, uint32_t> &input_map) {

  vector<uint32_t> taxa; // ho tutti gli indici dei nodi dell'albero che sono stati classificati. E' un vettore che contiene il numero di
                         // kmers e può essere 0 se non trova corrispondenza oppure taxon se c'è associazione taxonID-kmer
  vector<uint8_t> ambig_list; // k-mer che non si sovrapponono con le sequenze di dna. Contiene il numero di kmers e può essere 0 se il
                              // kmer è non ambiguo, e 1 se è ambiguo
  map<uint32_t, uint32_t> hit_counts; //mappa chiave-valore, la chiave è il taxonID ed il valore è il numero di k-mer associati all'id
  uint64_t *kmer_ptr; //casella del db in cui c'è il kmer
  uint32_t taxon = 0; // intero che rappresenta il numero di nodi nell'albero. All'inizio è 0 perchè albero vuoto.
                      // E' il taxonID associato al kmer
  uint32_t hits = 0;  // only maintained if in quick mode

  uint64_t current_bin_key;
  int64_t current_min_pos = 1;
  int64_t current_max_pos = 0;

  if (dna.seq.size() >= Database.get_k()) { // se la sequenza è più lunga del kmer
    KmerScanner scanner(dna.seq); // scorro la sequenza
    while ((kmer_ptr = scanner.next_kmer()) != NULL) { // mentre il puntatore trova una sequenza
      tot_kmer +=1; // contatore kmer totali
      taxon = 0; // taxonID iniziale
      if (scanner.ambig_kmer()) { // se kmer ambiguo non ho corrispondenza
        ambig_list.push_back(1); // metto 1 perchè ambiguo
      }
      else {
        ambig_list.push_back(0); // ho una corrispondenza tra la porzione di dna ed il k-mer
        uint32_t *val_ptr = Database.kmer_query(
                              Database.canonical_representation(*kmer_ptr),
                              &current_bin_key,
                              &current_min_pos, &current_max_pos
                            );
        taxon = val_ptr ? *val_ptr : 0; // se ho la corrispondenza inserisco la sequenza di dna nel rispettivo nodo dell'albero, quindi
                                        // associazione sequenza-taxonID. Altrimenti lascio 0
        if (taxon) { // se taxon non è 0.
          hit_counts[taxon]++; // conteggi locali. Popolo la mappa incrementando di uno i suoi elementi ad ogni iterazione del ciclo.
                               // In questo caso inserisco progressivamente i taxonID

          glob_hit_count[taxon]++; // conteggi globali
          tot_kmer_cl +=1; // contatore kmer classificati
          if (Quick_mode && ++hits >= Minimum_hit_count)
            break;
        }
      }
      taxa.push_back(taxon);
    }
  }

  /*
   * Da qui in poi nessuna modifica
   */

  uint32_t call = 0;
  if (Quick_mode)
    call = hits >= Minimum_hit_count ? taxon : 0;
  else
    call = resolve_tree(hit_counts, Parent_map);

  if (call)
    #pragma omp atomic
    total_classified++;

  if (Print_unclassified || Print_classified) {
    ostringstream *oss_ptr;
    ostringstream *oss_ptr2;
    if (call) {
      oss_ptr = &coss;
      oss_ptr2 = &coss2;
    }
    else {
      oss_ptr = &uoss;
      oss_ptr2 = &uoss2;
    }
    bool print = call ? Print_classified : Print_unclassified;
    if (print) {
      string delimiter = "|";
      if (Fastq_output && Output_format == "paired") {
	size_t delimiter_pos = 0;
	delimiter_pos = dna.header_line.find(delimiter);
	string header1 = dna.header_line.substr(0, delimiter_pos);
	string header2 = dna.header_line.substr(delimiter_pos + delimiter.length());
	delimiter_pos = dna.seq.find(delimiter);
	string seq1 = dna.seq.substr(0, delimiter_pos);
	string seq2 = dna.seq.substr(delimiter_pos + delimiter.length());
	delimiter_pos = dna.quals.find(delimiter);
	string quals1 = dna.quals.substr(0, delimiter_pos);
	string quals2 = dna.quals.substr(delimiter_pos + delimiter.length());
	(*oss_ptr) << "@" << header1 << endl
		   << seq1 << endl
		   << "+" << endl
		   << quals1 << endl;
	(*oss_ptr2) << "@" << header2 << endl
		    << seq2 << endl
		    << "+" << endl
		    << quals2 << endl;
      }
      else if (! Fastq_output && Output_format == "paired") {
	size_t delimiter_pos = 0;
	delimiter_pos = dna.header_line.find(delimiter);
	string header1 = dna.header_line.substr(0, delimiter_pos);
	string header2 = dna.header_line.substr(delimiter_pos + delimiter.length());
	delimiter_pos = dna.seq.find(delimiter);
	string seq1 = dna.seq.substr(0, delimiter_pos);
	string seq2 = dna.seq.substr(delimiter_pos + delimiter.length());
	(*oss_ptr) << ">" << header1 << endl
		   << seq1 << endl;
	(*oss_ptr2) << ">" << header2 << endl
		    << seq2 << endl;
      }
      else if (Fastq_output && Output_format == "legacy") {
	(*oss_ptr) << "@" << dna.header_line << endl
		   << dna.seq << endl
		   << "+" << endl
		   << dna.quals << endl;
      }
      else if (Fastq_output && Output_format == "interleaved") {
	size_t delimiter_pos = 0;
	delimiter_pos = dna.header_line.find(delimiter);
	string header1 = dna.header_line.substr(0, delimiter_pos);
	string header2 = dna.header_line.substr(delimiter_pos + delimiter.length());
	delimiter_pos = dna.seq.find(delimiter);
	string seq1 = dna.seq.substr(0, delimiter_pos);
	string seq2 = dna.seq.substr(delimiter_pos + delimiter.length());
	delimiter_pos = dna.quals.find(delimiter);
	string quals1 = dna.quals.substr(0, delimiter_pos);
	string quals2 = dna.quals.substr(delimiter_pos + delimiter.length());
	(*oss_ptr) << "@" << header1 << endl
		   << seq1 << endl
		   << "+" << endl
		   << quals1 << endl;
	(*oss_ptr) << "@" << header2 << endl
		   << seq2 << endl
		   << "+" << endl
		   << quals2 << endl;
      }
      else if (! Fastq_output && Output_format == "interleaved") {
	size_t delimiter_pos = 0;
	delimiter_pos = dna.header_line.find(delimiter);
	string header1 = dna.header_line.substr(0, delimiter_pos);
	string header2 = dna.header_line.substr(delimiter_pos + delimiter.length());
	delimiter_pos = dna.seq.find(delimiter);
	string seq1 = dna.seq.substr(0, delimiter_pos);
	string seq2 = dna.seq.substr(delimiter_pos + delimiter.length());
	(*oss_ptr) << ">" << header1 << endl
		   << seq1 << endl;
	(*oss_ptr) << ">" << header2 << endl
		   << seq2 << endl;
      }
      else if (! Fastq_output && Output_format == "legacy") {
	(*oss_ptr) << ">" << dna.header_line << endl
		   << dna.seq << endl;
      }
    }
  }

  if (! Print_kraken)
    return;

  if (call) {
    koss << "C\t";
  }
  else {
    if (Only_classified_kraken_output)
      return;
    koss << "U\t";
  }
  koss << dna.id << "\t" << call << "\t" << dna.seq.size() << "\t";
  size_t point =dna.id.find(".");
  string header = dna.id.substr(0,point);
  input_map[header]++;
  input_map["Somma"]++;



  if (Quick_mode) {
    koss << "Q:" << hits;
  }
  else {
    if (taxa.empty())
      koss << "0:0";
    else
      koss << hitlist_string(taxa, ambig_list);
  }

  koss << endl;
}

string hitlist_string(vector<uint32_t> &taxa, vector<uint8_t> &ambig)
{
  int64_t last_code;
  int code_count = 1;
  ostringstream hitlist;

  if (ambig[0])   { last_code = -1; }
  else            { last_code = taxa[0]; }

  for (size_t i = 1; i < taxa.size(); i++) {
    int64_t code;
    if (ambig[i]) { code = -1; }
    else          { code = taxa[i]; }

    if (code == last_code) {
      code_count++;
    }
    else {
      if (last_code >= 0) {
        hitlist << last_code << ":" << code_count << " ";
      }
      else {
        hitlist << "A:" << code_count << " ";
      }
      code_count = 1;
      last_code = code;
    }
  }
  if (last_code >= 0) {
    hitlist << last_code << ":" << code_count;
  }
  else {
    hitlist << "A:" << code_count;
  }
  return hitlist.str();
}

set<uint32_t> get_ancestry(uint32_t taxon) {
  set<uint32_t> path;

  while (taxon > 0) {
    path.insert(taxon);
    taxon = Parent_map[taxon];
  }
  return path;
}

void parse_command_line(int argc, char **argv) {
  int opt;
  long long sig;

  if (argc > 1 && strcmp(argv[1], "-h") == 0)
    usage(0);
  while ((opt = getopt(argc, argv, "d:i:t:u:n:r:m:o:qfFPcC:O:U:M")) != -1) {
    switch (opt) {
      case 'd' :
        DB_filename = optarg;
        break;
      case 'i' :
        Index_filename = optarg;
        break;
      case 't' :
        sig = atoll(optarg);
        if (sig <= 0)
          errx(EX_USAGE, "can't use nonpositive thread count");
        #ifdef _OPENMP
        if (sig > omp_get_num_procs())
          errx(EX_USAGE, "thread count exceeds number of processors");
        Num_threads = sig;
        omp_set_num_threads(Num_threads);
        #endif
        break;
      case 'n' :
        Nodes_filename = optarg;
        break;
      case 'r' :
        Names_filename = optarg;
        break;
      case 'q' :
        Quick_mode = true;
        break;
      case 'm' :
        sig = atoll(optarg);
        if (sig <= 0)
          errx(EX_USAGE, "can't use nonpositive minimum hit count");
        Minimum_hit_count = sig;
        break;
      case 'f' :
        Fastq_input = true;
        break;
      case 'F' :
        Fastq_output = true;
        break;
      case 'O' :
	Output_format = optarg;
	break;
      case 'c' :
        Only_classified_kraken_output = true;
        break;
      case 'C' :
        Print_classified = true;
        Classified_output_file = optarg;
        break;
      case 'U' :
        Print_unclassified = true;
        Unclassified_output_file = optarg;
        break;
      case 'o' :
        Kraken_output_file = optarg;
        break;
      case 'u' :
        sig = atoll(optarg);
        if (sig <= 0)
          errx(EX_USAGE, "can't use nonpositive work unit size");
        Work_unit_size = sig;
        break;
      case 'P' :
	Paired_input = true;
	break;
      case 'M' :
        Populate_memory = true;
        break;
      default:
        usage();
        break;
    }
  }

  if (DB_filename.empty()) {
    cerr << "Missing mandatory option -d" << endl;
    usage();
  }
  if (Index_filename.empty()) {
    cerr << "Missing mandatory option -i" << endl;
    usage();
  }
  if (Nodes_filename.empty() && ! Quick_mode) {
    cerr << "Must specify one of -q or -n" << endl;
    usage();
  }
  if (Names_filename.empty() && ! Quick_mode) {
    cerr << "Must specify one of -q or -r" << endl;
    usage();
  }
  if (optind == argc) {
    cerr << "No sequence data files specified" << endl;
  }
  if (Output_format == "paired" && (Classified_output_file == "-" || Unclassified_output_file == "-")) {
    cerr << "Can't send paired output to stdout" << endl;
    usage();
  }
  if ((Output_format == "paired" || Output_format == "interleaved") && ! Paired_input) {
    cerr << "Output format " << Output_format << " requires paired input" << endl;
    usage();
  }
  if (Output_format == "legacy" && Fastq_output) {
    cerr << "FASTQ output not supported for legacy ('N' delimited) output format. Use '--out-fmt paired'" << endl;
    usage();
  }
  if (Fastq_output && ! Fastq_input) {
    cerr << "FASTQ output requires FASTQ input" << endl;
    usage();
  }
}

void usage(int exit_code) {
  cerr << "Usage: classify [options] <fasta/fastq file(s)>" << endl
       << endl
       << "Options: (*mandatory)" << endl
       << "* -d filename      Kraken DB filename" << endl
       << "* -i filename      Kraken DB index filename" << endl
       << "  -n filename      NCBI Taxonomy nodes file" << endl
       << "  -o filename      Output file for Kraken output" << endl
       << "  -t #             Number of threads" << endl
       << "  -u #             Thread work unit size (in bp)" << endl
       << "  -q               Quick operation" << endl
       << "  -m #             Minimum hit count (ignored w/o -q)" << endl
       << "  -C filename      Print classified sequences" << endl
       << "  -U filename      Print unclassified sequences" << endl
       << "  -O format        [Un]classified output format {legacy, paired}" << endl
       << "  -f               Input is in FASTQ format" << endl
       << "  -F               Output in FASTQ format" << endl
       << "  -P               Input files are paired." << endl
       << "  -c               Only include classified reads in output" << endl
       << "  -M               Preload database files" << endl
       << "  -h               Print this message" << endl
       << endl
       << "At least one FASTA or FASTQ file must be specified." << endl
       << "Kraken output is to standard output by default." << endl;
  exit(exit_code);
}
